# 部署说明

## 第一步: 安装
```
npm install
```

### 第二步: 编译启动
```
npm run serve
```

### 第三步： 访问页面

![V1.0的架构图](../docs/images/d.jpg)



### 第三步: 打包

```
npm run build
```



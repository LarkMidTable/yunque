package com.larkmidtable.yunque.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DataxTransform {
    public static Map<String, Map<String, String>> transformJson(Map<String, JSONObject> dataxMap) {
        Map<String, Map<String, String>> ymap = new HashMap<>();
        Map<String, String> rmap = new HashMap<>();
        ymap.put("reader",rmap);
        Map<String, String> wmap = new HashMap<>();
        JSONObject jobJsonObject = dataxMap.get("job");
        ymap.put("writer",wmap);
        JSONArray contentJSONArray = jobJsonObject.getJSONArray("content");
        JSONObject contentJSONObject = contentJSONArray.getJSONObject(0);
        JSONObject readerJSONObject = contentJSONObject.getJSONObject("reader");

        JSONObject writerJSONObject = contentJSONObject.getJSONObject("writer");
        //
        String rname = readerJSONObject.getString("name");
        JSONObject rpJSONObject = readerJSONObject.getJSONObject("parameter");
        JSONArray rcJSONArray = rpJSONObject.getJSONArray("connection");
        JSONObject rcJSONObject =  rcJSONArray.getJSONObject(0);
        String rtable = (String)rcJSONObject.getJSONArray("table").get(0);
        String rurl =(String) rcJSONObject.getJSONArray("jdbcUrl").get(0);
        String rusername =rpJSONObject.getString("username");
        String rpassword =rpJSONObject.getString("password");
        StringBuffer rcolumnStringBuffer =new StringBuffer();
        JSONArray columnJSONArray = rpJSONObject.getJSONArray("column");
        for(int i=0;i<columnJSONArray.size();i++) {
            rcolumnStringBuffer.append(columnJSONArray.getString(i)+",");
        }
        String rcolumn = rcolumnStringBuffer.toString();
        rcolumn = rcolumn.substring(0,rcolumn.length()-1);
        rmap.put("plugin",rname);
        rmap.put("url",rurl);
        rmap.put("username",rusername);
        rmap.put("password",rpassword);
        rmap.put("table",rtable);
        rmap.put("column",rcolumn);
        rmap.put("thread","1");
        //
        String wname = writerJSONObject.getString("name");
        JSONObject wpJSONObject = writerJSONObject.getJSONObject("parameter");
        JSONArray wcJSONArray = wpJSONObject.getJSONArray("connection");
        JSONObject wcJSONObject =  wcJSONArray.getJSONObject(0);
        String wtable = (String)wcJSONObject.getJSONArray("table").get(0);
        String wurl =wcJSONObject.getString("jdbcUrl");
        String wusername =wpJSONObject.getString("username");
        String wpassword =wpJSONObject.getString("password");

        StringBuffer wcolumnStringBuffer =new StringBuffer();
        JSONArray wcolumnJSONArray = wpJSONObject.getJSONArray("column");
        for(int i=0;i<wcolumnJSONArray.size();i++) {
            wcolumnStringBuffer.append(wcolumnJSONArray.getString(i)+",");
        }
        String wcolumn = wcolumnStringBuffer.toString();
        wcolumn = wcolumn.substring(0,wcolumn.length()-1);
        wmap.put("plugin",wname);
        wmap.put("url",wurl);
        wmap.put("username",wusername);
        wmap.put("password",wpassword);
        wmap.put("table",wtable);
        wmap.put("column",wcolumn);
        wmap.put("thread","1");

        return ymap;
    }
}
